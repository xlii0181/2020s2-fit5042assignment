package fit5042.assignment.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import fit5042.assignment.repository.SystemRepository;
import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.Industry;
import fit5042.assignment.repository.entities.Users;




@ManagedBean(name = "systemManagedBean")
@SessionScoped
public class SystemManagedBean implements Serializable{
	@EJB
	SystemRepository systemRepository;
	

	public SystemManagedBean() {
		
	}
	
	

	public List<Users> getAllUsers() {
        try {
            List<Users> userList = systemRepository.getAllUsers();
            return userList;
        } catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
	
	
	
	public List<Customer> getAllcustomer(){
		try {
			List<Customer> customers = systemRepository.getAllCustomer();
			return customers;
		}catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
	}
	
	public List<ContactPerson> getAllContactPerson(){
		try {
			List<ContactPerson> persons = systemRepository.getAllContactPerson();
			return persons;
		}catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
	}
	
	public List<Industry> getAllIndustry(){
		try {
			List<Industry> industryList = systemRepository.getAllIndustry();
			return industryList;
		}catch (Exception ex) {
			Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	
	public void editCustomer(Customer customer) {
		try {			
			
			//customer.getIndustry().clear();
			
			int u = customer.getUser().getId();
			Users user = systemRepository.searchUsersById(u);
			customer.setUser(user);
			int i = customer.getIndustry().getId();
			Industry industry = systemRepository.searchIndustryById(i);
			customer.setIndustry(industry);
			systemRepository.editCustomer(customer);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));	
		}catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	public void editContact(ContactPerson person) {
		try {
			int c = person.getCustomer().getCustomerId();
			Customer customer = systemRepository.searchCustomerById(c);
			person.setCustomer(customer);
			systemRepository.editContactPerson(person);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact person has been updated succesfully"));	
		}catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		
		
	}
	
	public Customer searchCustomerById(int customerId) {
		try {
            return systemRepository.searchCustomerById(customerId);
        } catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
	}
	
	public List<Customer> searchCustomerByName(String customerName){
		try {
            //List<Customer> cs = systemRepository.searchCustomerByName(customerName);
			return systemRepository.searchCustomerByName(customerName);
        } catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
	}
	
	public Users searchUserById(int id){
		try {
            return systemRepository.searchUsersById(id);
        } catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
	}
	
	public void addContact(fit5042.assignment.controllers.ContactPerson localPerson) {
		ContactPerson p = convertPersonToentity(localPerson);
		try {
			
			systemRepository.addContactPerson(p);
		}catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

	}
	
	public void addCustomer(fit5042.assignment.controllers.Customer localCustomer) {
		Customer c = convertPersonToentity(localCustomer);
		try {
			
			systemRepository.addCustomer(c);
		}catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	public void addIndustry(fit5042.assignment.controllers.Industry localIndustry) {
		Industry i = convertIndustryToentity(localIndustry);
		try {
			systemRepository.addIndustry(i);
		}catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	private ContactPerson convertPersonToentity(fit5042.assignment.controllers.ContactPerson localPerson) {
		ContactPerson person = new ContactPerson();
		person.setAge(localPerson.getAge());
		
	    try {
	    	Customer c = systemRepository.searchCustomerById(localPerson.getCustomerId());
	    	person.setCustomer(c);
	    }catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		
		
		person.setEmail(localPerson.getEmail());
		person.setFirstName(localPerson.getFirstName());
		person.setLastName(localPerson.getLastName());
		person.setGender(localPerson.getGender());
		person.setPhoneNumber(localPerson.getPhoneNumber());
		person.setPersonId(localPerson.getPersonId());
		return person;
	}
	
	private Customer convertPersonToentity(fit5042.assignment.controllers.Customer localCustomer) {
		Customer customer = new Customer();
		try {
	    	Users u= systemRepository.searchUsersById(localCustomer.getUserId());
	    	customer.setUser(u);
	    }catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		try {
	    	Industry i = systemRepository.searchIndustryById(localCustomer.getIndustryId());
	    	customer.setIndustry(i);
	    }catch (Exception ex) {
            Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
		Address address = new Address();
		address.setCity(localCustomer.getCity());
		address.setNation(localCustomer.getNation());
		address.setPostcode(localCustomer.getPostcode());
		address.setState(localCustomer.getState());
		customer.setAddress(address);
		customer.setCustomerId(localCustomer.getCustomerId());
		customer.setCustomerName(localCustomer.getCustomerName());
		return customer;
		
	}
	
	private Industry convertIndustryToentity(fit5042.assignment.controllers.Industry localIndustry) {
		Industry i = new Industry();
		i.setType(localIndustry.getType());
		i.setId(localIndustry.getIndustryId());
		return i;
	}
	
	public void removeContact(int personId) {
		try {
			systemRepository.removeContactPerson(personId);
		}catch (Exception ex) {
			Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}
	
	public void removeCustomer(int customerId) {
		try {
			systemRepository.removeCustomer(customerId);
		}catch (Exception ex) {
			Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public void removeIndustry(int id) {
		try {
			systemRepository.removeIndustry(id);
		}catch (Exception ex) {
			Logger.getLogger(SystemManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	
	
	
	
	

}
