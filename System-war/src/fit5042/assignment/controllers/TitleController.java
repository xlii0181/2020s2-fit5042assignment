package fit5042.assignment.controllers;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named(value = "titleController")
@RequestScoped
public class TitleController {
	private String pageTitle;

    public TitleController() {
        // Set the page title 
        pageTitle = "Customer Managment System";
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

}
