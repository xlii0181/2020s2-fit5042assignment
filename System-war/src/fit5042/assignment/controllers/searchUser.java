package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.repository.entities.Users;

@RequestScoped
@Named("searchUser")
public class searchUser {
	private boolean showForm = true;
	private Users user;
	AdminApplication app;
	private int searchByInt;
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public int getSearchByInt() {
		return searchByInt;
	}
	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}
	public searchUser() {
		ELContext context
        = FacesContext.getCurrentInstance().getELContext();

		 app = (AdminApplication) FacesContext.getCurrentInstance()
				 .getApplication()
				 .getELResolver()
				 .getValue(context, null, "adminApplication");
		 
		 app.updateUserList();
		 
		 
	}
	
	public void searchUserById(int id) {
		try {
            //search this property then refresh the list in PropertyApplication bean
            app.searchUserById(id);
        } catch (Exception ex) {

        }
        showForm = true;
	}
	
	public void searchAll() {
		try {
            //search this property then refresh the list in PropertyApplication bean
            app.searchAllUser();
        } catch (Exception ex) {

        }
        showForm = true;
	}
	
	

}
