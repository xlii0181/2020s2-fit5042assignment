package fit5042.assignment.controllers;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "normalCustomerController")
@RequestScoped
public class NormalCustomerController {
	private int userId;
	private ArrayList<fit5042.assignment.repository.entities.Customer> customers;
	private ArrayList<fit5042.assignment.repository.entities.ContactPerson> contactPersons;
	private AdminApplication app;
	
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public NormalCustomerController() {
		customers = new ArrayList<>();
		contactPersons = new ArrayList<>();
		ELContext context
    	= FacesContext.getCurrentInstance().getELContext();

		app= (AdminApplication) FacesContext.getCurrentInstance()
            .getApplication()
            .getELResolver()
            .getValue(context, null, "adminApplication");
		userId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("ID"));
		customers = getCustomers();
		contactPersons = getContactPersons();
	}
	
	public ArrayList<fit5042.assignment.repository.entities.Customer> getCustomers(){
		if(customers.size() == 0) 
		{
			for(fit5042.assignment.repository.entities.Customer c : app.getCustomers()) {
				if(c.getUser().getId() == userId) {
					customers.add(c);
				}
			}
			return customers;
		}
		return customers;
		
	}
	
	public ArrayList<fit5042.assignment.repository.entities.ContactPerson> getContactPersons(){
		if(contactPersons.size() == 0) {
			for(fit5042.assignment.repository.entities.Customer c : customers) {
				for(fit5042.assignment.repository.entities.ContactPerson p : app.getContactPersons()) {
					if(p.getCustomer().getCustomerId() == c.getCustomerId())	
					contactPersons.add(p);
					
				}
			}
			return contactPersons;
		}
		return contactPersons;
	}

	

}
