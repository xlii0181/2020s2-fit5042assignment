package fit5042.assignment.controllers;

import java.util.ArrayList;


import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.SystemManagedBean;
import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.Industry;
import fit5042.assignment.repository.entities.Users;

@Named(value = "adminApplication")
@ApplicationScoped
public class AdminApplication {
	@ManagedProperty(value = "#{systemManagedBean}")
    SystemManagedBean systemManagedBean;
	
	private ArrayList<Users> userList;
	private ArrayList<Customer> customers;
	private ArrayList<ContactPerson> contactPersons;
	private ArrayList<Industry> industryList;
	
	private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }
    
    public AdminApplication() throws Exception{
    	userList = new ArrayList<>();
    	contactPersons = new ArrayList<>();
    	customers = new ArrayList<>();
    	industryList = new ArrayList<>();
    	ELContext elContext = FacesContext.getCurrentInstance().getELContext();
    	
    	systemManagedBean = (SystemManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "systemManagedBean");
    	
    	if (userList != null && userList.size() > 0){
            
        }else {
        	updateUserList();
        }
    	if(customers != null && customers.size() > 0) {
			
		}else {
			updateCustomers();
		}
    	if(contactPersons != null && contactPersons.size() > 0) {
			
		}else {
			updateContactPerson();
		}
    	if(industryList != null && industryList.size() > 0) {
    		
    	}else {
    		updateIndustryList();
    	}
    }
    
    
    public ArrayList<Users> getUserList() {
		return userList;
	}

	public void setUserList(ArrayList<Users> userList) {
		this.userList = userList;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
	
	public ArrayList<Industry> getIndustryList(){
		return industryList;
	}
	
	public void setIndustryList(ArrayList<Industry> industryList) {
		this.industryList = industryList;
	}

	public void updateUserList() {
    	if (userList != null && userList.size() > 0){
            
        }
    	else{
    		userList.clear();
    		for(fit5042.assignment.repository.entities.Users user : systemManagedBean.getAllUsers()) {
    			userList.add(user);
    		}
    		setUserList(userList);
    	}
    }
	
	public void updateCustomers() {
		if(customers != null && customers.size() > 0) {
			
		}
		else {
			customers.clear();
			for(fit5042.assignment.repository.entities.Customer customer : systemManagedBean.getAllcustomer()) {
    			customers.add(customer);
    		}
			//setCustomers(customers);
		}
		setCustomers(customers);
	}
	
	public void updateContactPerson() {
		if(contactPersons != null && contactPersons.size() > 0) {
			
		}else {
			contactPersons.clear();
			for(fit5042.assignment.repository.entities.ContactPerson person : systemManagedBean.getAllContactPerson()) {
    			contactPersons.add(person);
    		}
			
		}
		setContactPersons(contactPersons);
	}
	
	public void updateIndustryList() {
		if(industryList != null && industryList.size() > 0) {
			
		}else {
			industryList.clear();
			for(fit5042.assignment.repository.entities.Industry i : systemManagedBean.getAllIndustry()) {
				industryList.add(i);
			}
		}
		setIndustryList(industryList);
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	

	public ArrayList<ContactPerson> getContactPersons() {
		return contactPersons;
	}

	public void setContactPersons(ArrayList<ContactPerson> contactPersons) {
		this.contactPersons = contactPersons;
	}
	
	public void searchCustomerById(int customerId) {
		customers.clear();
		customers.add(systemManagedBean.searchCustomerById(customerId));
	}
	
	/**public void searchCustomerByName(String customerName) {
		customers.clear();
		for(fit5042.assignment.repository.entities.Customer c : systemManagedBean.searchCustomerByName(customerName)) {
			customers.add(c);
		}
		setCustomers(customers);
		
	}**/
	
	public void searchAll() {
		customers.clear();
		for(fit5042.assignment.repository.entities.Customer c : systemManagedBean.getAllcustomer()) {
			customers.add(c);
		}
	}
	
	public void searchUserById(int id) {
		userList.clear();
		userList.add(systemManagedBean.searchUserById(id));
	}
	
	public void searchAllUser() {
		userList.clear();
		for(fit5042.assignment.repository.entities.Users u : systemManagedBean.getAllUsers()) {
			userList.add(u);
		}
	}
	
	public void searchAllContact() {
		contactPersons.clear();
		for(fit5042.assignment.repository.entities.ContactPerson c : systemManagedBean.getAllContactPerson()) {
			contactPersons.add(c);
		}
	}
	
	public void searchAllIndustry() {
		industryList.clear();
		for(fit5042.assignment.repository.entities.Industry i : systemManagedBean.getAllIndustry()) {
			industryList.add(i);
		}
	}
	
	
	

	
}
