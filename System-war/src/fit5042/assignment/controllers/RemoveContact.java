package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.SystemManagedBean;

@RequestScoped
@Named("removeContact")
public class RemoveContact {
	
	@ManagedProperty(value = "#{systemManagedBean}")
	SystemManagedBean systemManagedBean;
	
	private boolean showForm = true;
	
	private ContactPerson contactPerson;
	
	AdminApplication app;

	public RemoveContact() {
		ELContext context
        	= FacesContext.getCurrentInstance().getELContext();
		app = (AdminApplication)FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "adminApplication");
		app.updateContactPerson();
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		systemManagedBean = (SystemManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "systemManagedBean");
	}
	
	public void removeContact(int personId) {
        try {
            //remove this property from db via EJB
            systemManagedBean.removeContact(personId);

            //refresh the list in PropertyApplication bean
             app.searchAllContact();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Property has been deleted succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;

    }

	
	
	

}
