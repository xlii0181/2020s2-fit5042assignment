package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.SystemManagedBean;
import fit5042.assignment.repository.entities.Customer;

@RequestScoped
@Named("removeCustomer")
public class RemoveCustomer {
	@ManagedProperty(value = "#{systemManagedBean}")
	SystemManagedBean systemManagedBean;
	private boolean showForm = true;
	private Customer customer;
	AdminApplication app;
	
	public RemoveCustomer() {
		ELContext context
    		= FacesContext.getCurrentInstance().getELContext();
		app = (AdminApplication)FacesContext.getCurrentInstance()
				.getApplication()
				.getELResolver()
				.getValue(context, null, "adminApplication");
		app.updateCustomers();
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		systemManagedBean = (SystemManagedBean) FacesContext.getCurrentInstance().getApplication()
				.getELResolver().getValue(elContext, null, "systemManagedBean");
	}
	
	public void removeCustomer(int customerId) {
		try {
            //remove this property from db via EJB
            systemManagedBean.removeCustomer(customerId);
            app.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been deleted succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;
	}
}
