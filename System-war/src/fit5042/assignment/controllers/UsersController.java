package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "usersController")
@RequestScoped
public class UsersController {
	private int id;
	private fit5042.assignment.repository.entities.Users user;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public UsersController(){
		id = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("ID"));
		user = getUser();
	}
	
	public fit5042.assignment.repository.entities.Users getUser(){
		if(user == null) {
			ELContext context
            = FacesContext.getCurrentInstance().getELContext();
			AdminApplication app
            	= (AdminApplication) FacesContext.getCurrentInstance()
                    	.getApplication()
                    	.getELResolver()
                    	.getValue(context, null, "adminApplication");
			return app.getUserList().get(--id);
		}
		return user;
	}
	

}
