package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.SystemManagedBean;

@Named(value = "addNormalContact")
@RequestScoped
public class AddNormalContact {
	@ManagedProperty(value = "#{systemManagedBean}")
	 SystemManagedBean systemManagedBean;
	 
	 private boolean showForm = true;
	 private ContactPerson person;
	 private int customerId;
	 AdminApplication app;
	 
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	public ContactPerson getPerson() {
		return person;
	}
	public void setPerson(ContactPerson person) {
		this.person = person;
	}
	
	public AddNormalContact() {
		
		
		ELContext context
			= FacesContext.getCurrentInstance().getELContext();

		app = (AdminApplication) FacesContext.getCurrentInstance()
				.getApplication()
				.getELResolver()
				.getValue(context, null, "adminApplication");
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		systemManagedBean = (SystemManagedBean) FacesContext.getCurrentInstance().getApplication()
				.getELResolver().getValue(elContext, null, "systemManagedBean");
		
		//app.updateContactPerson();
	}
	
	public void addContact(ContactPerson localPerson) {
		//localPerson.setCustomerId(1);
		try {
		
			systemManagedBean.addContact(localPerson);
			app.searchAllContact(); 
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact person has been added succesfully"));
		}catch (Exception ex) {

        }
        showForm = true;
	}
}
