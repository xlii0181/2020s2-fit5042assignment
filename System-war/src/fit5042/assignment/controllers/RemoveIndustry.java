package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.SystemManagedBean;

@RequestScoped
@Named("removeIndustry")
public class RemoveIndustry {
	@ManagedProperty(value = "#{systemManagedBean}")
	SystemManagedBean systemManagedBean;
	private boolean showForm = true;
	private Industry industry;
	AdminApplication app;
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	public Industry getIndustry() {
		return industry;
	}
	public void setIndustry(Industry industry) {
		this.industry = industry;
	}
	public RemoveIndustry() {
		ELContext context
			= FacesContext.getCurrentInstance().getELContext();
		app = (AdminApplication)FacesContext.getCurrentInstance()
				.getApplication()
				.getELResolver()
				.getValue(context, null, "adminApplication");
		app.updateCustomers();
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		systemManagedBean = (SystemManagedBean) FacesContext.getCurrentInstance().getApplication()
				.getELResolver().getValue(elContext, null, "systemManagedBean");
	}
	public void removeIndustry(int id) {
		try {
			systemManagedBean.removeIndustry(id);
			app.searchAllIndustry();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been deleted succesfully"));
		}catch (Exception ex) {

        }
        showForm = true;
	}
	
}
