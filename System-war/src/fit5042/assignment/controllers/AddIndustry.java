package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.SystemManagedBean;

@Named(value = "addIndustry")
@RequestScoped
public class AddIndustry {
	@ManagedProperty(value = "#{systemManagedBean}")
	 SystemManagedBean systemManagedBean;
	private boolean showForm = true;
	private Industry industry;
	AdminApplication app;
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	public Industry getIndustry() {
		return industry;
	}
	public void setIndustry(Industry industry) {
		this.industry = industry;
	}
	public AddIndustry(){
		ELContext context
    		= FacesContext.getCurrentInstance().getELContext();

		app = (AdminApplication) FacesContext.getCurrentInstance()
				.getApplication()
				.getELResolver()
				.getValue(context, null, "adminApplication");
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		systemManagedBean = (SystemManagedBean) FacesContext.getCurrentInstance().getApplication()
            		.getELResolver().getValue(elContext, null, "systemManagedBean");
	
	}
	public void addIndustry(Industry industry) {
		try {
			systemManagedBean.addIndustry(industry);
			app.searchAllIndustry();
		}catch (Exception ex) {

        }
        showForm = true;
	}
	
}
