package fit5042.assignment.controllers;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "normalContactController")
@RequestScoped
public class NormalContactController {
	private int customerId;
	private ArrayList<fit5042.assignment.repository.entities.ContactPerson> contactPersons;
	AdminApplication app;
	
	
	public int getCustomerId() {
		return customerId;
	}


	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}


	public NormalContactController() {
		contactPersons = new ArrayList<>();
		ELContext context
    	= FacesContext.getCurrentInstance().getELContext();

		app= (AdminApplication) FacesContext.getCurrentInstance()
            .getApplication()
            .getELResolver()
            .getValue(context, null, "adminApplication");
		customerId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerID"));
		app.updateContactPerson();
		
		contactPersons = getContactPersons();
	}
	
	public ArrayList<fit5042.assignment.repository.entities.ContactPerson> getContactPersons(){
			for(fit5042.assignment.repository.entities.ContactPerson p : app.getContactPersons()) {
				
						contactPersons.add(p);
					
				
				
		   }
			
		return contactPersons;
	}
	
	
	
}
