package fit5042.assignment.controllers;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import fit5042.assignment.repository.entities.Customer;

@Named(value = "normalContactPerson")
@RequestScoped
public class NormalContactPerson implements Serializable{
	private int personId;
	private String firstName;
	private String lastName;
	private int phoneNumber;
	private String email;
	private String age;
	private String gender;
	private Customer customer;
	private int customerId;
	
	
	public NormalContactPerson() {
		
	}
	public NormalContactPerson(int personId, String firstName, String lastName, int phoneNumber, String email,
			String age, String gender, Customer customer, int customerId) {
		
		this.personId = personId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.age = age;
		this.gender = gender;
		this.customer = customer;
		this.customerId = customerId;
	}
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	

}
