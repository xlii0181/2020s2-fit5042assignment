package fit5042.assignment.controllers;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named(value = "industry")
@RequestScoped
public class Industry implements Serializable{
	private int industryId;
	private String type;
	public Industry() {
		
	}
	public Industry(int industryId, String type) {
		
		this.industryId = industryId;
		this.type = type;
	}
	public int getIndustryId() {
		return industryId;
	}
	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
	
}
