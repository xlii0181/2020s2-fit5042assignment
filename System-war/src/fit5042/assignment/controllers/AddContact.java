package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.SystemManagedBean;

@Named(value = "addContact")
@RequestScoped
public class AddContact {
	 @ManagedProperty(value = "#{systemManagedBean}")
	 SystemManagedBean systemManagedBean;
	 
	 private boolean showForm = true;
	 private ContactPerson person;
	 AdminApplication app;
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	public ContactPerson getPerson() {
		return person;
	}
	public void setPerson(ContactPerson person) {
		this.person = person;
	}
	
	public AddContact() {
		ELContext context
        = FacesContext.getCurrentInstance().getELContext();

		app = (AdminApplication) FacesContext.getCurrentInstance()
				.getApplication()
				.getELResolver()
				.getValue(context, null, "adminApplication");
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		systemManagedBean = (SystemManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "systemManagedBean");
		//app.updateContactPerson();
	}
	
	public void addContact(ContactPerson localPerson) {
		try {
			systemManagedBean.addContact(localPerson);
			app.searchAllContact();
			//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact person has been added succesfully"));
		}catch (Exception ex) {

        }
        showForm = true;
	}
	
	

}
