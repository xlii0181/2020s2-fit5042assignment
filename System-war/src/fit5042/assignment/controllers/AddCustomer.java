package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.SystemManagedBean;

@Named(value = "addCustomer")
@RequestScoped
public class AddCustomer {
	@ManagedProperty(value = "#{systemManagedBean}")
	 SystemManagedBean systemManagedBean;
	private boolean showForm = true;
	private Customer customer;
	 AdminApplication app;
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public AddCustomer() {
		ELContext context
        	= FacesContext.getCurrentInstance().getELContext();

		app = (AdminApplication) FacesContext.getCurrentInstance()
				.getApplication()
				.getELResolver()
				.getValue(context, null, "adminApplication");
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		systemManagedBean = (SystemManagedBean) FacesContext.getCurrentInstance().getApplication()
                	.getELResolver().getValue(elContext, null, "systemManagedBean");
	}
	
	public void addCustomer(Customer customer) {
		try {
			systemManagedBean.addCustomer(customer);
			app.searchAll();
			//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact person has been added succesfully"));
		}catch (Exception ex) {

        }
        showForm = true;
	}
	

}
