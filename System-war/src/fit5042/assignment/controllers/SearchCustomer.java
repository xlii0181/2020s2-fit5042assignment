package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.repository.entities.Customer;

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
	private boolean showForm = true;
	private Customer customer;
	AdminApplication app;
	private int searchByInt;
	private String searchByName;
	public boolean isShowForm() {
		return showForm;
	}
	public void setShowForm(boolean showForm) {
		this.showForm = showForm;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public AdminApplication getApp() {
		return app;
	}
	public void setApp(AdminApplication app) {
		this.app = app;
	}
	public int getSearchByInt() {
		return searchByInt;
	}
	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}
	
	
	public String getSearchByName() {
		return searchByName;
	}
	public void setSearchByName(String searchByName) {
		this.searchByName = searchByName;
	}
	public SearchCustomer() {
		 ELContext context
         = FacesContext.getCurrentInstance().getELContext();

		 app = (AdminApplication) FacesContext.getCurrentInstance()
				 .getApplication()
				 .getELResolver()
				 .getValue(context, null, "adminApplication");
		 
		 app.updateCustomers();

	}
	
	/*public void searchCustomerById(int customerId) {
        try {
            //search this property then refresh the list in PropertyApplication bean
            app.searchCustomerById(customerId);
        } catch (Exception ex) {

        }
        showForm = true;

    }*/
	
	/*public void searchCustomerByName(String customerName) {
		try {
            
            app.searchCustomerByName(customerName);
        } catch (Exception ex) { 

        }
        showForm = true;
	}*/
	
	public void searchAll() {
		try {
            
            app.searchAll();
        } catch (Exception ex) { 

        }
	}
	
	

}
