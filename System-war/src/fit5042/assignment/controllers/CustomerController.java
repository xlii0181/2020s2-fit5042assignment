package fit5042.assignment.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;



@Named(value = "customerController")
@RequestScoped
public class CustomerController {
	private int customerId;
	private fit5042.assignment.repository.entities.Customer customer;
	private ArrayList<String> industryList;
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	
	
	public CustomerController() {
		customerId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerID"));
		industryList = new ArrayList<>();
		fillIndustry();
		customer = getCustomer();
		
	}
	
	public fit5042.assignment.repository.entities.Customer getCustomer(){
		if (customer == null) {
            // Get application context bean PropertyApplication 
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
           
            AdminApplication app
                    = (AdminApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "adminApplication");
            // -1 to propertyId since we +1 in JSF (to always have positive property id!) 
            return app.getCustomers().get(--customerId); //this propertyId is the index, don't confuse with the Property Id
        }
        return customer;
	}
	
	public void fillIndustry() {
		industryList.clear();
		industryList.add("Bank");
		industryList.add("Building");
		industryList.add("Data communication");
		industryList.add("Education");
		industryList.add("Health");
		industryList.add("Mining");
		industryList.add("Publish");
	}
	public ArrayList<String> getIndustryList() {
		return industryList;
	}
	public void setIndustryList(ArrayList<String> industryList) {
		this.industryList = industryList;
	}
	
	

}
