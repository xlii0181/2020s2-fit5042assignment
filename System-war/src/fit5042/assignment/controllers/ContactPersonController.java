package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "contactPersonController")
@RequestScoped
public class ContactPersonController {
	private int personId;
	private fit5042.assignment.repository.entities.ContactPerson contactPerson;
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public ContactPersonController() {
		personId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("personID"));
		contactPerson = getContactPerson();
	}
	
	public fit5042.assignment.repository.entities.ContactPerson getContactPerson(){
		if(contactPerson == null) {
			ELContext context
            	= FacesContext.getCurrentInstance().getELContext();
			AdminApplication app
            	= (AdminApplication) FacesContext.getCurrentInstance()
                    	.getApplication()
                    	.getELResolver()
                    	.getValue(context, null, "adminApplication");
			return app.getContactPersons().get(--personId);
		}
		return contactPerson;
	}
}
