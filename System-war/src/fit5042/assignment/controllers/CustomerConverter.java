package fit5042.assignment.controllers;

import java.util.List;

import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import fit5042.assignment.mbeans.SystemManagedBean;
import fit5042.assignment.repository.entities.Customer;


@FacesConverter(forClass = fit5042.assignment.repository.entities.Customer.class, value = "customer" )
public class CustomerConverter implements Converter{
	
	 @ManagedProperty(value = "#{systemManagedBean}")
	 SystemManagedBean systemManagedBean;
	 
	 public List<Customer> customerDB;
	 
	 public CustomerConverter() {
		 try {
			 ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			 systemManagedBean = (SystemManagedBean) FacesContext.getCurrentInstance().getApplication()
		                .getELResolver().getValue(elContext, null, "systemManagedBean");
				customerDB = systemManagedBean.getAllcustomer();
		 }catch (Exception ex) {

	     }
	 }
	
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
		
		if (submittedValue.trim().equals("")) {
            return null;
        } else {
            try {
                int number = Integer.parseInt(submittedValue);

                for (Customer c : customerDB) {
                    if (c.getCustomerId() == number) {
                        return c;
                    }
                }

            } catch (NumberFormatException exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid customer"));
            }
        }

        return null;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
		if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((Customer) value).getCustomerId());
        }
    
	}

}
