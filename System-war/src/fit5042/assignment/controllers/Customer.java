package fit5042.assignment.controllers;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import fit5042.assignment.repository.entities.Address;
import fit5042.assignment.repository.entities.Industry;
import fit5042.assignment.repository.entities.Users;

@Named(value = "customer")
@RequestScoped
public class Customer implements Serializable{
	private int customerId;
	private String customerName;
	private Industry industry;
	private Address address;
	private Users user;
	private int userId;
	private int industryId;
	private String city;
	private String nation;
	private String state;
	private int postcode;
	
	
	
	
	public Customer() {
		
	}
	
	
	public Customer(int customerId, String customerName, Industry industry, Address address, Users user, int userId,
			int industryId, String city, String nation, String state, int postcode) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.industry = industry;
		this.address = address;
		this.user = user;
		this.userId = userId;
		this.industryId = industryId;
		this.city = city;
		this.nation = nation;
		this.state = state;
		this.postcode = postcode;
	}


	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Industry getIndustry() {
		return industry;
	}
	public void setIndustry(Industry industry) {
		this.industry = industry;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getIndustryId() {
		return industryId;
	}
	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getPostcode() {
		return postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}
	
	

}
