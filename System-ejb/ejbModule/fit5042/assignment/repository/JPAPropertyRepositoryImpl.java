package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.Industry;
import fit5042.assignment.repository.entities.Users;

@Stateless
public class JPAPropertyRepositoryImpl implements SystemRepository{
	@PersistenceContext(unitName = "System-ejbPU")
	private EntityManager entityManager;
	

	@Override
	public void addUsers(Users user) throws Exception {
		List<Users> usersList = entityManager.createNamedQuery(Users.GET_ALL_USERS).getResultList();
		user.setId(usersList.get(0).getId() + 1);
		entityManager.persist(user);
		
	}

	@Override
	public void addCustomer(Customer customer) throws Exception {
		
		List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_CUSTOMER).getResultList();
		if(customers.size() < 1) {
			customer.setCustomerId(1);
		}else {
			customer.setCustomerId(customers.get(0).getCustomerId() + 1);
		}
		entityManager.persist(customer);
		
	}

	@Override
	public void addContactPerson(ContactPerson contactPerson) throws Exception {
		// TODO Auto-generated method stub
		List<ContactPerson> persons = entityManager.createNamedQuery(ContactPerson.GET_ALL_CONTACT_PERSON).getResultList();
		if(persons.size() < 1) {
			contactPerson.setPersonId(1);
		}else {
			contactPerson.setPersonId(persons.get(0).getPersonId() + 1);
		}
		entityManager.persist(contactPerson);
		
	}

	@Override
	public Customer searchCustomerById(int customerId) throws Exception {
		// TODO Auto-generated method stub
		Customer customer = entityManager.find(Customer.class, customerId);
		return customer;
	}

	@Override
	public ContactPerson searchContactPersonById(int personId) throws Exception {
		// TODO Auto-generated method stub
		ContactPerson contactPerson = entityManager.find(ContactPerson.class, personId);
		return contactPerson;
	}

	@Override
	public Users searchUsersById(int id) throws Exception {
		// TODO Auto-generated method stub
		Users user = entityManager.find(Users.class, id);
		return user;
	}

	@Override
	public List<Users> getAllUsers() throws Exception {
		// TODO Auto-generated method stub
		return entityManager.createNamedQuery(Users.GET_ALL_USERS).getResultList();
	}

	@Override
	public List<Customer> getAllCustomer() throws Exception {
		// TODO Auto-generated method stub
		return entityManager.createNamedQuery(Customer.GET_ALL_CUSTOMER).getResultList();
	}

	@Override
	public List<ContactPerson> getAllContactPerson() throws Exception {
		// TODO Auto-generated method stub
		return entityManager.createNamedQuery(ContactPerson.GET_ALL_CONTACT_PERSON).getResultList();
	}

	@Override
	public void editUsers(Users user) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.merge(user);
		}catch (Exception e) {
			
		}
		
	}
	
	

	@Override
	public void editCustomer(Customer customer) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.merge(customer);
		}catch (Exception e) {
			
		}
		
	}

	@Override
	public void editContactPerson(ContactPerson contactPerson) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.merge(contactPerson);
		}catch (Exception e) {
			
		}
		
	}

	@Override
	public void removeUsers(int id) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.remove(searchUsersById(id));
		}catch (Exception e) {
			
		}
		
	}

	@Override
	public void removeCustomer(int customerId) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.remove(searchCustomerById(customerId));
		}catch (Exception e) {
			
		}
	}

	@Override
	public void removeContactPerson(int personId) throws Exception {
		// TODO Auto-generated method stub
		try {
			entityManager.remove(searchContactPersonById(personId));
		}catch (Exception e) {
			
		}
	}

	@Override
	public List<Customer> searchCustomerByName(String customerName) throws Exception {
		// TODO Auto-generated method stub
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
    	CriteriaQuery<Customer> query = builder.createQuery(Customer.class);
    	Root<Customer> w = query.from(Customer.class);
    	query.select(w);
        Predicate predicate = builder.equal(w.get("state"), customerName);
    	query.where(predicate);
        return entityManager.createQuery(query).getResultList();
		
		
	}

	@Override
	public void addIndustry(Industry industry) throws Exception {
		// TODO Auto-generated method stub
		List<Industry> industryList = entityManager.createNamedQuery(Industry.GET_ALL_INDUSTRY).getResultList();
		if(industryList.size() < 1) {
			industry.setId(1);
		}else {
			industry.setId(industryList.get(0).getId() + 1);
		}
		entityManager.persist(industry);
		
	}

	@Override
	public List<Industry> getAllIndustry() throws Exception {
		// TODO Auto-generated method stub
		return entityManager.createNamedQuery(Industry.GET_ALL_INDUSTRY).getResultList();
		
	}

	@Override
	public void removeIndustry(int id) throws Exception {
		
		try {
			entityManager.remove(searchIndustryById(id));
		}catch (Exception e) {
			
		}
	}

	@Override
	public Industry searchIndustryById(int id) throws Exception {
		Industry industry = entityManager.find(Industry.class, id);
		return industry;
		
	}

}
