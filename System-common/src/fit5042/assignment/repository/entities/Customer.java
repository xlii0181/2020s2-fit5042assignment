package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOMER")
@NamedQueries({
    @NamedQuery(name = Customer.GET_ALL_CUSTOMER, query = "SELECT c FROM Customer c order by c.customerId desc")})

public class Customer implements Serializable{
	
	public static final String GET_ALL_CUSTOMER = "Customer.getAll";
	private int customerId;
	private String customerName;
	private Industry industry;
	private Address address;
	private Set<ContactPerson> contacts;
	private Users user;
	
	public Customer() {
		
	}
	
	public Customer(int customerId, String customerName, Industry industry, Address address,
			Set<ContactPerson> contacts, Users user) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.industry = industry;
		this.address = address;
		this.contacts = contacts;
		this.user = user;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	@Column(name = "customer_name")
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	@ManyToOne
	public Industry getIndustry() {
		return industry;
	}
	
	public void setIndustry(Industry industry) {
		this.industry = industry;
	}
	
	@OneToMany(mappedBy = "customer")
	public Set<ContactPerson> getContacts() {
		return contacts;
	}
	public void setContacts(Set<ContactPerson> contacts) {
		this.contacts = contacts;
	}
	
	@Embedded
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	
	@ManyToOne
	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Customer {customerId=" + customerId + ", customerName=" + customerName + ", industry=" + industry
				+ ", address=" + address + ", contacts=" + contacts + "}";
	}
	
	

	
	
	

}
