package fit5042.assignment.repository.entities;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Access(AccessType.PROPERTY)	
public class Address implements Serializable{
	private String city;
	private String state;
	private int postcode;
	private String nation;
	
	public Address() {}
	public Address(String city, String state, int postcode, String nation) {
		super();
		this.city = city;
		this.state = state;
		this.postcode = postcode;
		this.nation = nation;
	}
	
	@Column(name = "city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name = "state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name = "postcode")
	public int getPostcode() {
		return postcode;
	}
	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}
	
	@Column(name = "nation")
	public String getNation() {
		return nation;
	}
	
	public void setNation(String nation) {
		this.nation = nation;
	}
	@Override
	public String toString() {
		return city + ", " + state + " " + postcode + ", " + nation;
	}
	
	
	

	
}
