package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "INDUSTRY")
@NamedQueries({
    @NamedQuery(name = Industry.GET_ALL_INDUSTRY, query = "SELECT i FROM Industry i order by i.id desc")})
public class Industry implements Serializable{
	public static final String GET_ALL_INDUSTRY = "Industry.getAll";
	private int id;
	private String type;
	private Set<Customer> customers;
	
	
	
	public Industry() {
		
	}

	public Industry(int id, String type, Set<Customer> customers) {
		super();
		this.id = id;
		this.type = type;
		this.customers = customers;
	}
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@OneToMany(mappedBy = "industry")
	public Set<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}
	
	@Override
	public String toString() {
		return type;
	}

	
	
	
	

}
