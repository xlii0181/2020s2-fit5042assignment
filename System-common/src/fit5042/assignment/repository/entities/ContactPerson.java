package fit5042.assignment.repository.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "CONTACT_PERSON")
@NamedQueries({
    @NamedQuery(name = ContactPerson.GET_ALL_CONTACT_PERSON, query = "SELECT p FROM ContactPerson p order by p.personId desc")})
public class ContactPerson implements Serializable{
	public static final String GET_ALL_CONTACT_PERSON = "ContactPerson.getAll";

	private int personId;
	private String firstName;
	private String lastName;
	private int phoneNumber;
	private String email;
	private String age;
	private String gender;
	private Customer customer;
	
	public ContactPerson() {}
	
	
	public ContactPerson(int personId, String firstName, String lastName, int phoneNumber, String email, String age,
			String gender, Customer customer) {
		super();
		this.personId = personId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.age = age;
		this.gender = gender;
		this.customer = customer;
	}
	
	@Id
    @GeneratedValue
    @Column(name = "contact_person_id")
	public int getPersonId() {
		return personId;
	}
	
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	
    @Column(name = "first_name")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
    @Column(name = "last_name")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
    @Column(name = "phone_number")
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
    @Column(name = "email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
    @Column(name = "age")
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}

    @Column(name = "gender")
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@ManyToOne
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	@Override
    public String toString() {
        return this.personId + " - " + lastName + " " + firstName + " - " + customer.getCustomerName();
    }
	
	

}
