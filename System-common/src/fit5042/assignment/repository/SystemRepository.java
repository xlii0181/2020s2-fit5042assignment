package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Remote;

import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.Industry;
import fit5042.assignment.repository.entities.Users;

@Remote
public interface SystemRepository {
	
	public void addUsers(Users user) throws Exception;
	
	public void addCustomer(Customer customer) throws Exception;
	
	public void addContactPerson(ContactPerson contactPerson) throws Exception;
	
	public void addIndustry(Industry industry) throws Exception;
	
	public Customer searchCustomerById(int customerId) throws Exception;
	
	public ContactPerson searchContactPersonById(int personId) throws Exception;
	
	public Industry searchIndustryById(int id) throws Exception;
	
	public Users searchUsersById(int id) throws Exception;
	
	public List<Users> getAllUsers() throws Exception;
	
	public List<Customer> getAllCustomer() throws Exception;
	
	public List<ContactPerson> getAllContactPerson() throws Exception;
	
	public List<Industry> getAllIndustry() throws Exception;
	
	public void editUsers(Users user) throws Exception;
	
	public void editCustomer(Customer customer) throws Exception;
	
	public void editContactPerson(ContactPerson contactPerson) throws Exception;
	
	public void removeUsers(int id) throws Exception;
	
	public void removeCustomer(int customerId) throws Exception;
	
	public void removeContactPerson(int personId) throws Exception;
	
	public void removeIndustry(int id) throws Exception;
	
	//public void editIndustry(Customer customer) throws Exception;
	
	public List<Customer> searchCustomerByName(String customerName) throws Exception;

}
